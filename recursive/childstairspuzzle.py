#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Stairs Climbing Puzzle

Question: A child is climb­ing up a stair­case with n steps, and can hop either 
1 step, 2 steps, or 3 steps at a time. Imple­ment a method to count how many 
pos­si­ble ways the child can jump up the stairs.

Created on Tue Nov  7 21:39:28 2017

@author: apple
"""

def possible_ways(n):
    if n < 1:
        return 0
    else:
        return 1 + possible_ways(n-1) + possible_ways(n-2) + possible_ways(n-3) 

if __name__ == '__main__':   
    ways = possible_ways(3)

    print(ways)