#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Print All the Subsets of a Given Set (Power Set)

Given a set of num­bers, print all the poss­si­ble sub­sets of it includ­ing empty set.

Example:
    S ={1,2,3}

P   S(S): {{}, {1}, {2}, {3}, {1, 2}, {1, 3}, {2, 3}, {1, 2, 3}}.

Created on Tue Nov  7 22:24:51 2017

@author: Lilun
"""

# recursive
def find_all_subsets(s, this_set, total_set):
    if len(this_set) == len(s):
        return
    else:
        for e in s:
            if e not in this_set:
                local_set = this_set[:]
                local_set.append(e)
                if sorted(local_set) not in total_set:
                    total_set.append(sorted(local_set))
                find_all_subsets(s,local_set,total_set)
                
        return
    
if __name__ == '__main__':
    s = [1,2,3, 4]
    this_set = []
    total_set = []
    total_set.append([])
    find_all_subsets(s,this_set,total_set)
    print(total_set)
    print(len(total_set))
            
            

