#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Print All Possible Subsets with Sum equal to a given Number

Objec­tive: Given a num­ber N, Write an algo­rithm to print all pos­si­ble sub­sets 
with Sum equal to N.

Sample input: 4
output:
    1111
    112
    121
    13
    211
    22
    31
    4    

Created on Mon Nov  6 21:18:25 2017

@author: Lilun Cao
"""

def print_all_subsets_with_same_sum(n, subset):
    if n == 0:
        print(subset)
    else:
        for i in range(1, n+1):
            new_subset = subset + str(i)
            print_all_subsets_with_same_sum(n-i,new_subset)
            
if __name__ == '__main__':           
    subset = ''
    print_all_subsets_with_same_sum(4, subset)
