#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
The Coin Change Problem
You have m types of coins available in infinite quantities where the value of each 
coin is given in the array . Can you determine the number of ways of making 
change for n=3 units using the given types of coins? For example, if m=4, and c=[8,3,1,2], 
we can make change for  units in three ways: {1,1,1}, {1,2}, and {3}.

Given n, m, and ,C print the number of ways to make change for  n units using any 
number of coins having the values given in C.

Input Format

The first line contains two space-separated integers describing the respective 
values of  n and m. 
The second line contains  m space-separated integers describing the respective 
values of c_0, c_1, ..., c_m-1 (the list of distinct coins available in infinite amounts).

Constraints

1 <= c_i <= 50
1 <= n <= 250
1 <= m <= 50
Each c_i is guaranteed to be distinct


Each  is guaranteed to be distinct.

Hints

Solve overlapping subproblems using Dynamic Programming (DP): 
You can solve this problem recursively but will not pass all the test cases without optimizing to eliminate the overlapping subproblems. Think of a way to store and reference previously computed solutions to avoid solving the same subproblem multiple times.
Consider the degenerate cases: 
How many ways can you make change for 0 cents?
How many ways can you make change for > 0 cents if you have no coins?
If you're having trouble defining your solutions store, then think about it in terms of the base case (n=0).
The answer may be larger than a 32-bit integer.
Output Format

Print a long integer denoting the number of ways we can get a sum of n from the given infinite supply of m types of coins.

Sample Input 0

4 3
1 2 3
Sample Output 0

4
Explanation 0

There are four ways to make change for n=4 using coins with values given by C=[1,2,3]:

Thus, we print 4 as our answer.

Sample Input 1

10 4
2 5 3 6
Sample Output 1

5

Explanation 1

There are five ways to make change for n=10 units using coins with values given 
by C=[2,5,3,6]:



Thus, we print 5 as our answer.

Created on Wed Nov  1 23:49:46 2017

@author: Lilun Cao
"""

#!/bin/python3

import sys

total = []

def getWays(n, c, a):
    # Complete this function
    if n == 0:
        found = False
        a.sort()
        if a not in total:
            total.append(a)
        return 1
    elif n > 0:
        if not c:
            return 0
        else:
            ways = 0
            for i in c:
                b = a[:]
                b.append(i)
                ways = ways + getWays(n-i,c,b)
            return ways
    else:
        return 0
if __name__ == '__main__':
    n, m = input().strip().split(' ')
    n, m = [int(n), int(m)]
    c = list(map(int, input().strip().split(' ')))
    # Print the number of ways of making change for 'n' units using coins having the values given by 'c'
    a = []
    ways = getWays(n, c,a)
    #print(total)
    print(len(total))