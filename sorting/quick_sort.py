#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
quick_sort
Created on Fri Oct 27 20:29:12 2017

@author: Lilun Cao
"""

def partition(arr, i, j):
    pivot = arr[j]  # pivot = last element in the array
    left = i
    right = j - 1
    #print("pivot=%d" % pivot)
    while left <= right:
        while arr[left] <= pivot and left <= right:
            left = left + 1
            
        while arr[right] >= pivot and left <= right:
            right = right - 1
         
        #print("left=%d" % left)
        #print("right=%d" % right)
        if left < right:
            arr[left], arr[right] = arr[right], arr[left]           
            
    arr[j], arr[left] = arr[left], arr[j]
    #print("partition index=%d" % left)
    #print("arr=" + str(arr))
    return left
    
def quick_sort(arr, begin, end):
    if begin < end:
        pivot = partition(arr, begin, end)
        quick_sort(arr, begin, pivot-1)
        quick_sort(arr, pivot + 1, end)
       
if __name__ == '__main__':
    arr = [2,4,6,9,20,68, 23, 12,26, 22, 16, 11]
    #arr = [54, 26, 93, 17, 77, 31, 44, 55, 20]
    quick_sort(arr,0,len(arr)-1)
    print(arr)
            