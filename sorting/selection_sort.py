#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This program demos how to do the sorting of a list using selection sorting algorithm
It has BigO(n^2) efficiency
Created on Sun Oct 22 14:58:23 2017

@author: Lilun Cao
"""

def selection_sort(aList):
    """The input argument aList is a list of numbers. This function will return a 
    sorted list of numbers after the traditional selection sorting algorithm is implemented.
    This algorithm is not efficient compared to other algorithms like merge sort.
    """
    if not aList:
        raise ValueError("aList can not be empty")
        
    try:
        if type(aList) != 'list':
            aList = list(aList)
            
        count = len(aList)
        if count <= 1:
            return aList
        
        for i in range(count):
            max_number = aList[0]
            max_index = 0
            for j in range(1, count - i):
                if aList[j] > max_number:
                    max_number = aList[j]
                    max_index = j
            
            if max_index != count - i:
                aList[count-i-1], aList[max_index] = aList[max_index], aList[count-i-1]
                    
    except TypeError:
        raise TypeError("The input argument aList can not be converted to a list object")
        
    return aList

if __name__ == '__main__':
    a = [3, 6, 8 ,9 ,25,20, 59, 26]
    b = selection_sort(a)
    print(b)
    c = (25,20)
    d = selection_sort(c)
    print(d)
    import random
    random.seed()
    e = [random.randint(1,100) for n in range(50)]
    f = selection_sort(e)
    print(f)