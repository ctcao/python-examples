#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
merge_sort
Created on Fri Oct 27 22:28:00 2017

@author: Lilun Cao
"""

def merge_sort(arr):
    if len(arr) <= 1:
        return arr
    mid = len(arr) // 2
    left_half = arr[:mid]
    right_half = arr[mid:]
    merge_sort(left_half)
    merge_sort(right_half)
    
    merge(left_half, right_half, arr)

def merge(a1, a2, arr):
    l = 0
    r = 0
    a = 0
    while l < len(a1) and r < len(a2):
        if a1[l] < a2[r]:
            arr[a] = a1[l]
            l = l + 1
        else:
            arr[a] = a2[r]
            r = r + 1
        a = a + 1
        
    while l < len(a1):
        arr[a] = a1[l]
        l = l + 1
        a = a + 1
    
    while r < len(a2):
        arr[a] = a2[r]
        a = a + 1
        r = r + 1
            
if __name__ == '__main__':
    #arr = [2,4,6,9,20,68, 23, 12,26, 22, 16, 11]
    arr = [54, 26, 93, 17, 77, 31, 44, 55, 20]
    merge_sort(arr)
    print(arr)