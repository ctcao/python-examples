#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Merge K Sorted Arrays

Given k sorted array, write an algo­rithm to merge Them into One sorted array.

A[0] = new int[] { 1, 5, 8, 9 };
A[1] = new int[] { 2, 3, 7, 10 };
A[2] = new int[] { 4, 6, 11, 15 };
A[3] = new int[] { 9, 14, 16, 19 };
A[4] = new int[] { 2, 4, 6, 9 };

Output:
[1, 2, 2, 3, 4, 4, 5, 6, 6, 7, 8, 9, 9, 9, 10, 11, 14, 15, 16, 19]

Created on Tue Nov  7 22:06:57 2017

@author: apple
"""
from queue import PriorityQueue

A = []

A.append([1,5,8,9])
A.append([2,3,7,10])
A.append([4,6,11,15])
A.append([9,14,16,19])
A.append([2,4,6,9])

q = PriorityQueue()
for a in A:
    for i in a:
        q.put(i)
     
# the integers will be displayed in the sorted order        
while not q.empty():
    print(q.get())
        


