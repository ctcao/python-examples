#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
The Word Break Problem

Given an string and a dic­tio­nary of words, find out if the input 
string can be bro­ken into a space-separated sequence of one or more dic­tio­nary 
words.

Exam­ple:

dictionary = ["I" , "have", "Jain", "Sumit", "am", "this", "dog"]

String = "IamSumit"

Output: "I am Sumit"

String ="thisisadog"

Output : String can't be broken

Created on Mon Nov  6 22:18:23 2017

@author: Lilun Cao
"""

def find_a_sequence_of_words(seq, the_dict):
    word = ''
    words = []
    for c in seq:
        word = word + c
        if word in the_dict:
            words.append(word)
            if seq.endswith(word):
                print(words)
                return True
            word = ''
     
    
    return False

if __name__ == '__main__':
    dictionary = ["I" , "have", "Jain", "Sumit", "am", "this", "dog"]

    #aString = "IamSumit"
    aString ="thisisadog"

    yes = find_a_sequence_of_words(aString, dictionary)
    print(yes)

    
