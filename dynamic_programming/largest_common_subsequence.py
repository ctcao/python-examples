#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Problem : Longest Increasing subsequence
The Longest Increasing Subsequence problem is to find the longest increasing 
subsequence of a given sequence. Given a sequence S= {a1 , a2 , a3, 
a4, ............., an-1, an } we have to find a longest subset such that 
for all j and i,  j<i in the subset aj<ai.
First of all we have to find the value of the longest subsequences(LSi) at 
every index i with last element of sequence being ai. Then largest LSi would 
be the longest subsequence in the given sequence. To begin LSi is assigned to 
be one since ai is element of the sequence(Last element). Then for all j such 
that j<i and aj<ai ,we find Largest LSj and add it to LSi. Then algorithm 
take O(n2) time.

Created on Wed Nov  1 22:10:02 2017

@author: Lilun Cao
"""

def find_longest_increasing_subsequence(a_number):
    num_length = len(a_number)
    memos = [0] * num_length
    for i in range(num_length):
        memos[i] = 1
        if i > 0:
            for j in range(i):
                if a_number[i] > a_number[j] and memos[i] <= memos[j]:
                    memos[i] = memos[j] + 1
        #print(memos[i])
    largest = max(memos)
    return largest
 
if __name__ == '__main__':    
    a = [2,5,1,3,6,8,4]
    largest = find_longest_increasing_subsequence(a)
    print(largest)

