#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Maximum Sum of All Sub-arrays 
A sub-array has one number of some continuous numbers. Given an integer array 
with positive numbers and negative numbers, get the maximum sum of all sub-arrays. 
Time complexity should be O(n). 
For example, in the array {1, -2, 3, 10, -4, 7, 2, -5}, 
its sub-array {3, 10, -4, 7, 2} has the maximum sum 18.

Created on Sat Nov  4 07:55:59 2017

@author: Lilun

"""
# Kadane’s Algorithm — Maximum Subarray Problem

def find_max_subarray(a):
    max_so_far = 0
    max_ending_here = 0

    for i in range(len(a)):
        max_ending_here = max_ending_here + a[i]
        if max_ending_here < 0:
            max_ending_here = 0
        if max_so_far < max_ending_here:
            max_so_far = max_ending_here
    return max_so_far

if __name__ == '__main__':
    a = [-2,1,-3,4,-1,2,1]
    max_sum = find_max_subarray(a)
    print(max_sum)

    