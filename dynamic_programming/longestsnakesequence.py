#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Find longest Snake sequence in a given matrix

Given two dimen­sional matrix, write an algo­rithm to find out the snake sequence 
which has the max­i­mum length. There could be many snake sequence in the matrix, 
you need to return the one with the max­i­mum length. Travel is allowed only in 
two direc­tions, either go right OR go down.

Snake sequence can be made if num­ber in adja­cent right 
cell or num­ber in the adja­cent down cell is either +1 or –1 from the num­ber 
in the cur­rent cell.

Exam­ple:
           1   2   1   2
           7   7   2   5
           6   4   3   4
           1   2   2   5
           
        Snake sequence:
            1  2  1  2
            5  4  5
            4  3  4  5
            1  2
            1  2  1  2  3  4  5  - longest snake sequence

Created on Thu Nov  9 21:52:53 2017

@author: Lilun cao
"""

######recursive way###################

def issnakecell(currcell, adjacentcell):
    return (adjacentcell == currcell + 1) or (adjacentcell == currcell - 1)

def islistinanolist(A, X):
  for i in range(len(X) - len(A) + 1):
    if A == X[i:i+len(A)]: 
        return True
  return False

def findsnakesequence(matrix, row, col, rowlen, collen, snake, allsnake):

    if col + 1 <= collen-1 or row+1 <= rowlen-1:
        if col+1 <= collen-1 and issnakecell(matrix[row][col], matrix[row][col+1]):
            if snake:
                tempsnake = snake[:]
            else:
                tempsnake = []
            tempsnake.append(matrix[row][col+1])
            findsnakesequence(matrix, row, (col+1), rowlen, collen,tempsnake,allsnake)
        if row+1 <= rowlen-1 and issnakecell(matrix[row][col], matrix[row+1][col]):
            if snake:
                tempsnake = snake[:]
            else:
                tempsnake = []
            tempsnake.append(matrix[row+1][col])
            findsnakesequence(matrix, (row+1), col, rowlen, collen,tempsnake,allsnake)
    else:
        # check if any list in allsnake contains snake. Add it only if no.
        inanybiglist = False
        for al in allsnake:
            if islistinanolist(snake,al):
               inanybiglist = True
               break
        if not inanybiglist:
            allsnake.append(snake)
        return
    
##########dynamic programming################
def getmaxsequence(matrix):
    rowlen = len(matrix)
    collen = len(matrix[0])
    maxlen = 1
    maxrow =0
    maxcol = 0

    result = []
    arow = [1] * collen
    for i in range(rowlen):
        newrow = arow[:]
        result.append(newrow)
        
    for i in range(rowlen):
        for j in range(collen):
            if i > 0 and (matrix[i][j] - matrix[i-1][j]) in [-1,1]:
                result[i][j] = max(result[i][j], result[i-1][j]+1)
                if maxlen < result[i][j]:
                    maxlen = result[i][j]
                    maxrow = i
                    maxcol = j
            if j > 0 and (matrix[i][j] - matrix[i][j-1]) in [-1,1]:
                result[i][j] = max(result[i][j], result[i][j-1]+1)
                if maxlen < result[i][j]:
                    maxlen = result[i][j]
                    maxrow = i
                    maxcol = j
                    
    print("Max Snake Sequence: %d" % maxlen)
    printseqpath(matrix, result, maxlen, maxrow, maxcol)
    
def printseqpath(matrix, result, maxlen, maxrow, maxcol):
    templen = maxlen
    maxpath = []
    while templen >= 1:
        maxpath.insert(0, matrix[maxrow][maxcol])
        if maxrow > 0 and (result[maxrow-1][maxcol] - result[maxrow][maxcol]) == -1:
            maxrow = maxrow - 1
        elif maxcol > 0 and (result[maxrow][maxcol-1] - result[maxrow][maxcol]) == -1:
            maxcol = maxcol - 1
        templen = templen - 1
    print(maxpath) 
  
if __name__ == '__main__':      
    matrix = [[1, 2, 1, 2],
           [7, 7, 2, 5],
           [6, 4, 3, 4],
           [1, 2, 2, 5]]

    # recursive way
    whichways = ['recursive','dynamic']

    whichway = whichways[1]

    if whichway == 'recursive':
        allsnake = []
        rowlen = len(matrix)
        collen = len(matrix[0])
        for i in range(rowlen):
            for j in range(collen):
                snake = []
                snake.append(matrix[i][j])
                findsnakesequence(matrix,i,j,rowlen,collen, snake, allsnake)
        
        #find largest snake sequence
        largestseq = []
        for alist in allsnake:
            if len(alist) > len(largestseq):
                largestseq = alist
        print(largestseq)
    elif whichway == 'dynamic':
        getmaxsequence(matrix)
        
        


