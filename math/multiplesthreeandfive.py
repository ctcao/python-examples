#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 
6 and 9. The sum of these multiples is 23.

Find the sum of all the multiples of 3 or 5 below 1000.

Created on Sun Oct 22 22:26:41 2017

@author: Lilun Cao
"""

def multiples_three_and_five(limit_number):
    result = set()
    n1 = limit_number // 3
    for i in range(1,n1):
        result.add(i * 3)
        
    if n1 * 3 < limit_number:
        result.add(n1 * 3)
        
    n2 = limit_number // 5
    for j in range(1, n2):
        result.add(j * 5)
      
    if n2 * 5 < limit_number:
        result.add(n2 * 5)
    return sorted(result)

if __name__ == '__main__':
    a = multiples_three_and_five(10)
    print(a)
    