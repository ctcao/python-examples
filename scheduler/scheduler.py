#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created this scheduler for my son so that he can follow his schedule
when he uses the computer instead of hanging up there for a long time. This works
like a school ringing system. When it is time to change his class or take a break, he will
receive an alert message popped up to remind him that he needs to do something different 
now.

Created on Sun Oct 22 15:56:50 2017

@author: Lilun Cao
"""
from threading import Thread
import time

def split_time(mins, length=5):
    times = mins // length
    remainder = mins % length
    return times, remainder

class Scheduler(Thread):
    def __init__(self, name, programs, task_length, break_length):
        """
        The programs list contains a list of tasks that need to be done in sequence
        just like school classes need to be taken sequentially.
        The input arguments task_length and break_length are all in minutes
        """
        Thread.__init__(self)
        self.name = name
        self.programs = programs
        self.task_length = task_length
        self.break_length = break_length
        
    
    def run(self):
        remind_time = 2
        for program in self.programs:
            print("Starting program " + program)
            frequency, remainder = split_time(self.task_length,remind_time)
            passed = 0
            for n in range(frequency):
                time.sleep(remind_time * 60)
                passed = passed + remind_time
                print("Your program " + program + " has " + str(self.task_length-passed) + " minutes left")
            time.sleep(remainder * 60)
            print("Time for your program " + program + " is over. Go stretch your body and take " + str(self.break_length) + " minutes's break")
            time.sleep(self.break_length * 60)
            if program != self.programs[-1]:
                print("Break time is over. Start working!!!")
            else:
                print("Your job is done. Time to sleep")
            time.sleep(10)
            
if __name__ == "__main__":
    programs = ("Spanish", "Mathematics", "Biology", "History", "Literature")
    task_length = 5
    break_length = 2
    thread_name = "Freshmen's Scheduler"
    scheduler = Scheduler(thread_name, programs, task_length, break_length)
    scheduler.start()
            
    

